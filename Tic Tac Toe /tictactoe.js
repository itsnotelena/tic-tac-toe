var origBoard;
var player = "O";
const winCombos = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[6, 4, 2]
]

const cells = document.querySelectorAll('.cell');
const messageElement = document.getElementById("message");
const startButton = document.getElementById("start-game");
//startButton.addEventListener("click",startGame);

/**
 * Function that initiates the game
 */
function startGame() {
  startButton.classList.add("hidden");
  messageElement.innerHTML = `Player '${player}' can click a tile`;
	origBoard = Array.from(Array(9).keys());
	for (var i = 0; i < cells.length; i++) {
		cells[i].innerText = ''; //not add anything in the cells
		cells[i].style.removeProperty('background-color'); //highlight each square
		cells[i].addEventListener('click', turnClick); //call the turnClick function
	}
}

/**
 * Function that pass in the click event
 * @param {*} square 
 */
function turnClick(square) {
  clickTile(square.target.id, player);
  changePlayer();
  //checkTie()
}

/**
 * Function that check if there is a winner 
 * @param {*} board 
 * @param {*} player 
 */
function checkWin(board, player) {
	let plays = board.reduce((a, e, i) => 
		(e === player) ? a.concat(i) : a, []);
	let gameWon = null;
	for (let [index, win] of winCombos.entries()) {
		if (win.every(elem => plays.indexOf(elem) > -1)) {
			gameWon = {index: index, player: player};
			break;
		}
	}
	return gameWon;
}

/**
 * Event handler (that still should be assigned to the elements) to handle when
 * the user clicks an tile in the grid
 */
function clickTile(squareId) {
  // change the elements in the HTML
  if(origBoard[squareId] != "O" && origBoard[squareId] != "X") {
  origBoard[squareId] = player;
  document.getElementById(squareId).innerText = player; //add X or O in each cells 
  }
  // detect if X or O has won 
  // if not, change 'currentPlayer'
  let gameWon = checkWin(origBoard, player);
  if (gameWon) {
    gameOver(gameWon);
    winner(player);
  }
}

/**
 * Function that change the 'currentPlayer' 
 */
function changePlayer() {
  if(player == "O") {
    player = "X";
  } else if(player == "X") {
    player = "O";
  }  
}

/**
 * Call this function if one of the players has won
 */
function winner(player) {
  if (player == "X") {
    messageElement.innerHTML = "Well done X!!";
  } else if (player == "O") {
    messageElement.innerHTML = "Great job O!!";
  }
  document.body.classList.add("winner");
}

/**
 * Call this function when there is a winner 
 * @param {*} gameWon 
 */
function gameOver(gameWon) {
	for (let index of winCombos[gameWon.index]) {
		document.getElementById(index).style.backgroundColor =
			(gameWon.player == "O") ? "#C7EA46" : "#98FB98";
	}
	for (var i = 0; i < cells.length; i++) {
		cells[i].removeEventListener('click', turnClick, false);
	}
}

  // optional feature if you have time
  /**
   * Call this function when restart button is pressed
   */
  function reset() {
  const restartButton = document.getElementById("reset");
  restartButton.addEventListener("click", startGame());
  document.body.classList.remove("winner");
  }

  // function checkTie(){
  //  for(i = 0; i < cells.length; i++){
  //    if(!cells.length && !checkWin(origBoard,player)){
  //      cells[i].style.backgroundColor = "green"; 
  //      messageElement.innerHTML = `It's a tie!`;
  //    }
  //    return true; 
  //  }
  //  return false; 
  // }